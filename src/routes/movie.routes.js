const express = require('express')
const router = express.Router()
const MovieController = require('../controllers/movie.controller')

// Lijst van movies
router.post('/', MovieController.createMovie)
router.get('/', MovieController.getAllMovies)
router.get('/:movieId', MovieController.getMovieById)

module.exports = router
